<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Kategori;

class KategoriController extends Controller
{
    public function getKategori(Request $request){
        $kategoriModel = new Kategori;
        $kategori = $kategoriModel->getAllKategori();

        return response()->json([
            'data' => $kategori
        ]);
    }
}