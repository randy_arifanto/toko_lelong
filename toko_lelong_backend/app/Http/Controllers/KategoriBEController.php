<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Kategori;
use Session;
use Redirect;

class KategoriBEController extends Controller
{    
    public function index(Request $request){
    	$kategoriModel = new Kategori;
    	$kategori = $kategoriModel->getAllKategori();

    	return view('kategori.list')
				->with('kategori',$kategori);
    }

    public function add(Request $request){
    	return view('kategori.add');
    }

    public function addact(Request $request){
    	// Validate request
    	request()->validate([
            'name' => 'required',
        ]);

        //insert data
    	$kategoriModel = new Kategori;
        $kategoriModel->nama = $request->input('name');
        $kategoriModel->save();

    	return Redirect::route('kategori');
    }

    public function edit(Request $request, $id){
        $kategoriModel = Kategori::find($id)->toarray();
        return view('kategori.edit')
                ->with('kategori',$kategoriModel);
    }

    public function editact(Request $request){
        // Validate request
        request()->validate([
            'name' => 'required',
        ]);


        // Save Reseller
        $kategoriModel = Kategori::find($request->input('id'));
        $kategoriModel->nama = $request->input('name');
        $kategoriModel->save();

        return Redirect::route('kategori');
    }
}
