<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Produk;

class ProdukController extends Controller
{
    public function getProduk(Request $request){
        $produkModel = new Produk;
        $produk = [];

        if ($request->has('q') && !$request->has('k')) {
            $produk = $produkModel->getProdukWQ($request->query('q'));
        } elseif (!$request->has('q') && $request->has('k')) {
            $produk = $produkModel->getProdukWK($request->query('k'));
        } elseif ($request->has('q') && $request->has('k')) {
            $produk = $produkModel->getProdukWQK($request->query('q'), $request->query('k'));
        } else {
            $produk = $produkModel->getAllProduk();
        }

        return response()->json([
            'data' => $produk
        ]);
    }

    public function getDetailProduk(Request $request){
        $produkModel = new Produk;
        $produk = [];
        
        if ($request->has('i')) {
            $produk = $produkModel->getProduk($request->query('i'));
        }

        return response()->json([
            'data' => $produk
        ]);
    }
}