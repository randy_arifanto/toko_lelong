<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pesanan;
use App\Models\Pesananproduk;
use Session;
use Redirect;

class OrderController extends Controller
{    
    public function index(Request $request){
    	$pesananModel = new Pesanan;
    	$pesanan = $pesananModel->getAllPesanan();

    	return view('order.list')
				->with('order',$pesanan);
    }    

    public function view(Request $request, $id){
        $pesananProdukModel = new Pesananproduk;
        $pesananProduk = $pesananProdukModel->detailPesananBE($id);

        return view('order.view')
                ->with('pesananproduk',$pesananProduk);;
    }    

    public function order_status_ready(Request $request){
        $pesananModel = Pesanan::find($request->input('id'));
        $pesananModel->status = "Pesanan Siap";
        $pesananModel->save();

        return Redirect::route('view-order', ['id' => $request->input('id')]);
    }    

    public function order_status_done(Request $request){
        $pesananModel = Pesanan::find($request->input('id'));
        $pesananModel->status = "Pesanan Selesai";
        $pesananModel->save();

        return Redirect::route('view-order', ['id' => $request->input('id')]);
    }
}
