<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Produk;
use App\Models\Kategori;
use Session;
use Redirect;

class ProdukBEController extends Controller
{    
    public function index(Request $request){
    	$produkModel = new Produk;
    	$produk = $produkModel->getAllProdukBE();

    	return view('produk.list')
				->with('produk',$produk);
    }

    public function add(Request $request){
        $kategoriModel = new Kategori;
        $kategori = $kategoriModel->getAllKategori();
    	return view('produk.add')
                ->with('kategori', $kategori);
    }

    public function addact(Request $request){
    	// Validate request
    	request()->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
            'uploadFile' => 'required',
        ]);

        //Upload Image
        $imageName = time() . '.' . $request->file('uploadFile')->getClientOriginalExtension();
        $request->file('uploadFile')->move('images', $imageName);

        //insert data
    	$produkModel = new Produk;
        $produkModel->nama = $request->input('nama');
        $produkModel->deskripsi = $request->input('deskripsi');
        $produkModel->harga = $request->input('harga');
        $produkModel->gambar = $imageName;
        $produkModel->id_kategori = $request->input('id_kategori');
        $produkModel->save();

    	return Redirect::route('produk');
    }

    public function edit(Request $request, $id){
        $produk = Produk::find($id)->toarray();
        $kategoriModel = new Kategori;
        $kategori = $kategoriModel->getAllKategori();
        return view('produk.edit')
                ->with('produk', $produk)
                ->with('kategori', $kategori);
    }

    public function editact(Request $request){
        // Validate request
        request()->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required|numeric',
        ]);

        // Update data
        $produkModel = Produk::find($request->input('id'));
        $produkModel->nama = $request->input('nama');
        $produkModel->deskripsi = $request->input('deskripsi');
        $produkModel->harga = $request->input('harga');
        $produkModel->id_kategori = $request->input('id_kategori');
        if (!empty($request->file('uploadFile'))) {
            $imageName = time() . '.' . $request->file('uploadFile')->getClientOriginalExtension();
            $request->file('uploadFile')->move('images', $imageName);
            $produkModel->gambar = $imageName;
        }
        $produkModel->save();

        return Redirect::route('produk');
    }
}
