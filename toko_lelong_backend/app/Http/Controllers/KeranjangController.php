<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

use App\Models\Keranjang;

class KeranjangController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function getKeranjang(Request $request){
        $keranjangModel = new Keranjang;
        $keranjang = $keranjangModel->getKeranjangWU(auth()->user()['id']);

        return response()->json([
            'data' => $keranjang
        ]);
    }

    public function addKeranjang(Request $request){
        $validator = Validator::make($request->all(), [
            'id_produk' => 'required',
            'jumlah' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {
            
            $keranjangModel = new Keranjang;

            // Save Keranjang
            $keranjangModel->id_user = auth()->user()['id'];
            $keranjangModel->id_produk = $request->id_produk;
            $keranjangModel->jumlah = $request->jumlah;
            $keranjangModel->save();
            
            return response()->json([
                'message' => 'ok'
            ]);
            
        } catch (Exception $e) {
            return response()->json([
                'message' => 'not ok'
            ], 500);            
        }


    }
}