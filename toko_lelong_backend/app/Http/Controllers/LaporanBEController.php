<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Pesanan;
use Session;
use Redirect;
use DateTime;

class LaporanBEController extends Controller
{    
    public function index(Request $request){
    	$pesananModel = new Pesanan;
    	$pesanan = $pesananModel->getPesananAgregat()->toarray();
    	$pesanan_json = $pesananModel->getPesananAgregat()->toJson();
		$filter_applied = "Semua Data";
		
		if (!empty($request->query('start_date')) && !empty($request->query('end_date'))) {
			$start_date = $request->query('start_date');
			$end_date = $request->query('end_date');
			$pesanan = $pesananModel->getPesananAgregatWithFilter($start_date, $end_date)->toarray();
			$pesanan_json = $pesananModel->getPesananAgregatWithFilter($start_date, $end_date)->toJson();
			$filter_applied = $start_date . ' - ' . $end_date;
		}

    	return view('laporanorder.list')
				->with('pesanan',$pesanan)
				->with('pesanan_json',$pesanan_json)
				->with('filter_applied',$filter_applied);
    }

    public function filter(Request $request){
		$date = $request->input('daterange_filter');
		$date_arr = array_map('trim', explode('-', $date));
		
		$start_date = DateTime::createFromFormat('d/m/Y', $date_arr[0]);
		$new_start_date = $start_date->format('Y-m-d');
		$new_start_date = $new_start_date . " 00:00:00";
		
		$end_date = DateTime::createFromFormat('d/m/Y', $date_arr[1]);
		$new_end_date = $end_date->format('Y-m-d');
		$new_end_date = $new_end_date . " 23:59:59";

    	return Redirect::route('laporan',['start_date' => $new_start_date, 'end_date' => $new_end_date]);
    }
}
