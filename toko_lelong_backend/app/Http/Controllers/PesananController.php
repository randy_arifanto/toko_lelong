<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Pesanan;
use App\Models\Pesananproduk;
use App\Models\Keranjang;
use App\Models\Produk;

class PesananController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function getPesanan(Request $request){
        $pesananModel = new Pesanan;
        $pesananProdukModel = new Pesananproduk;
        $pesanan = [];
        $pesananDetail = [];
        
        $pesanan = $pesananModel->getPesananWU(auth()->user()['id']);

        foreach ($pesanan as $key => $value){
            $detail = $pesananProdukModel->detailPesanan($value['id']);
            $temp = $value;
            $temp['detail'] = $detail;
            array_push($pesananDetail, $temp);
        }

        return response()->json([
            'data' => $pesananDetail
        ]);
    }

    public function getDetailPesanan(Request $request){
        $pesananModel = new Pesanan;
        $pesanan = [];

        if ($request->has('i')) {
            $pesanan = $pesananModel->getPesanan($request->query('i'));
        }

        return response()->json([
            'data' => $pesanan
        ]);
    }

    public function addPesanan(Request $request){
        $keranjangModel = new Keranjang;
        $keranjang = $keranjangModel->getKeranjangWU(auth()->user()['id']);

        $total_harga = 0;
        $produkModel = new Produk;
        
        foreach ($keranjang as $key => $value) {
            $produk = $produkModel->getProduk($value['id_produk']);
            $total_harga_temp = $value['jumlah'] * $produk['harga'];
            $total_harga = $total_harga + $total_harga_temp;
        }

        if ($total_harga == 0) {
            return response()->json([
                'message' => 'not ok'
            ], 500);
        }
        
        DB::beginTransaction();

        try {
            $newPesanan = Pesanan::create([
                        'id_user' => auth()->user()['id'],
                        'kode_order' => 'TLO'.auth()->user()['id'].date('mdYhis', time()),
                        'status' => 'Pesanan Masuk',
                        'total_harga' => $total_harga]);
        } catch(ValidationException $e)
        {
            DB::rollback();
        } catch(\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'message' => 'not ok'
            ], 500);  
        }

        try {
            foreach ($keranjang as $keypp => $valuepp) {
                $produk_for_pp = $produkModel->getProduk($valuepp['id_produk']);
                $total_harga_for_pp = $valuepp['jumlah'] * $produk_for_pp['harga'];
                $newPesananProduk = Pesananproduk::create([
                    'id_pesanan' => $newPesanan->id,
                    'id_produk' => $valuepp['id_produk'],
                    'jumlah' => $valuepp['jumlah'],
                    'total_harga' => $total_harga_for_pp,
                ]);
            }
        } catch(ValidationException $e)
        {
            DB::rollback();
        } catch(\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'message' => 'not ok'
            ], 500);  
        }
        DB::commit();

        $keranjang = $keranjangModel->clear(auth()->user()['id']);
            
        return response()->json([
            'message' => 'ok'
        ]);
    }
}