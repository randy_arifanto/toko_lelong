<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pesanan extends Model
{
    protected $table = 'pesanan';
    protected $primaryKey = 'id';
    protected $fillable = ['id_user', 'kode_order', 'status', 'total_harga'];

    public function getAllPesanan(){
        return $this::all()->toarray();
    }

    public function getPesanan($id){
        $result = $this::find($id);
        if (!empty($result)) {
            return $this::find($id)->toarray();;
        }
        return [];
    }

    public function getPesananWU($id_user){
        $result = $this
                ->where('id_user', '=', $id_user)
                ->orderby('created_at', 'DESC')
                ->get();
        return $result->toarray();
    }

    public function getPesananAgregat(){
        $result = $this
                ->select('status', DB::raw('count(*) as total'), DB::raw('sum(total_harga) as total_harga'))
                ->groupby('status')
                ->get();
        return $result;
    }

    public function getPesananAgregatWithFilter($start_date, $end_date){
        $result = $this
                ->select('status', DB::raw('count(*) as total'), DB::raw('sum(total_harga) as total_harga'))
                ->where('created_at', '>=', $start_date)
                ->where('created_at', '<=', $end_date)
                ->groupby('status')
                ->get();
        return $result;
    }
}