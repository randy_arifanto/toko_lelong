<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'id';

    public function getAllProduk(){
        return $this::all()->toarray();
    }

    public function getAllProdukBE(){
        $result = $this
                ->leftjoin('kategori', 'kategori.id', '=', 'produk.id_kategori')
                ->select('produk.*', 'kategori.nama as nama_kategori')
                ->get();
        return $result->toarray();
    }

    public function getProduk($id){
        $result = $this::find($id);
        if (!empty($result)) {
            return $result->toarray();
        }
        return [];
    }

    public function getProdukWQ($keyword){
        $result = $this
                ->where('nama', 'like', '%'.$keyword.'%')
                ->get();
        return $result->toarray();
    }

    public function getProdukWK($id_kategori){
        $result = $this
                ->where('id_kategori', '=', $id_kategori)
                ->get();
        return $result->toarray();
    }

    public function getProdukWQK($keyword, $id_kategori){
        $result = $this
                ->where('nama', 'like', '%'.$keyword.'%')
                ->where('id_kategori', '=', $id_kategori)
                ->get();
        return $result->toarray();
    }
}