<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pesananproduk extends Model
{
    protected $table = 'pesananproduk';
    protected $primaryKey = 'id';
    protected $fillable = ['id_pesanan', 'id_produk', 'jumlah', 'total_harga'];

    public function detailPesanan($id){
        $result = $this
                ->leftjoin('pesanan', 'pesananproduk.id_pesanan', '=', 'pesanan.id')
                ->leftjoin('produk', 'pesananproduk.id_produk', '=', 'produk.id')
                ->select('produk.*', 'pesananproduk.jumlah')
                ->where('pesanan.id', '=', $id)
                ->get();
        return $result->toarray();
    }

    public function detailPesananBE($id){
        $result = $this
                ->leftjoin('pesanan', 'pesananproduk.id_pesanan', '=', 'pesanan.id')
                ->leftjoin('produk', 'pesananproduk.id_produk', '=', 'produk.id')
                ->select('pesanan.id as id_pesanan', 'pesanan.kode_order', 'pesanan.status', 'pesanan.total_harga', 'produk.*', 'pesananproduk.jumlah')
                ->where('pesanan.id', '=', $id)
                ->get();
        return $result->toarray();
    }
}