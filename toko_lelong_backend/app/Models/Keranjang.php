<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Keranjang extends Model
{
    protected $table = 'keranjang';
    protected $primaryKey = 'id';

    public function getAllKeranjang(){
        return $this::all()->toarray();
    }

    public function getKeranjangWU($id_user){
        $result = $this
                ->leftjoin('produk', 'keranjang.id_produk', '=', 'produk.id')
                ->select('keranjang.*', 'produk.nama', 'produk.harga', 'produk.gambar')
                ->where('keranjang.id_user', '=', $id_user)
                ->get();
        return $result->toarray();
    }
    
    public function clear($id_user){
        if (!is_null($id_user)) {
            $this->where('id_user', '=', $id_user)->delete();
        }
    }
}