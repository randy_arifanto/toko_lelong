<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'id';

    public function getAllKategori(){
        $result = $this
                ->where('aktif', '=', 'y')
                ->get();
        return $result->toarray();
    }
}