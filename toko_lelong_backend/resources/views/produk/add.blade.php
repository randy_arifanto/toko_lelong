@extends('layouts/app')

@section('title', 'Produk Management')

@section('content')
<!-- MAIN CONTENT -->
<div class="main">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Produk Management') }}</h3>
					<p class="panel-subtitle"><a href="{{ route('produk') }}">{{ __('Produk List') }}</a> > {{ __('Add Produk') }}</p>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				@foreach ($errors->all() as $error)
				<i class="fa fa-times-circle"></i> {{ $error }}<br>
				@endforeach
			</div>
			@endif
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Add Produk') }}</h3>
				</div>
				<div class="panel-body">
					<form id="add-produk-form" method="POST" action="{{ route('add-produk') }}" enctype="multipart/form-data">
            			@csrf
						<label>{{ __('Nama Produk') }}</label>
						<input type="text" name="nama" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" placeholder="Nama Produk" value="{{ old('nama') }}">
						<br>
						<label>{{ __('Deskripsi') }}</label>
						<input type="text" name="deskripsi" class="form-control{{ $errors->has('deskripsi') ? ' is-invalid' : '' }}" placeholder="Deskripsi" value="{{ old('deskripsi') }}">
						<br>
						<label>{{ __('Harga') }}</label>
						<input type="number" name="harga" class="form-control{{ $errors->has('harga') ? ' is-invalid' : '' }}" placeholder="Harga" value="{{ old('harga') }}">
						<br>
						<label>{{ __('Gambar') }}</label>
            			<input type="file" id="uploadFile" name="uploadFile" required="">
						<br>
						<label>{{ __('Kategori') }}</label>
						<select name="id_kategori" class="form-control">
					        @foreach($kategori as $value)
   								<option value="{{$value['id']}}">{{$value['nama']}}</option>
							@endforeach
   						</select>
					</form>
				</div>
			</div>
			<div class="panel panel-headline">
				<div class="panel-heading">
					<a href="{{ route('produk') }}" class="simple-button">Cancel</a>
					<a href="{{ route('add-produk') }}" class="simple-button" onclick="event.preventDefault(); document.querySelector('form#add-produk-form').submit();">Submit</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN CONTENT -->
@endsection