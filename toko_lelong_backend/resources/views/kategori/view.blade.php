@extends('layouts/app')

@section('title', 'Order Management')

@section('content')
<!-- MAIN CONTENT -->
<div class="main">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Order Management') }}</h3>
					<p class="panel-subtitle"><a href="{{ route('order') }}">{{ __('Order List') }}</a> > {{ __('Order Detail') }}</p>
				</div>
			</div>
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Order Detail') }}</h3>
					<h3 class="panel-title">{{ __('Kode Order:') }} {{$pesananproduk[0]['kode_order']}}</h3>
					<h3 class="panel-title">{{ __('Status:') }} {{$pesananproduk[0]['status']}}</h3>
				</div>
				<div class="panel-body">
					<table id="example" class="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Deskripsi</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody>
				        <?php $i = 1; ?>
				        @foreach($pesananproduk as $value)
							<tr>
								<td>{{ $value['nama'] }}</td>
								<td>{{ $value['deskripsi'] }}</td>
								<td>{{ $value['harga'] }}</td>
							</tr>
				        <?php $i++; ?>
						@endforeach
							<tr>
								<td colspan="2">Total Harga</td>
								<td>{{$pesananproduk[0]['total_harga']}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
            <form id='order-ready-form' method="POST" action="{{ route('order-ready') }}"><input type="hidden" name="id" value="{{$pesananproduk[0]['id_pesanan']}}"> @csrf </form>
            <form id='order-done-form' method="POST" action="{{ route('order-done') }}"><input type="hidden" name="id" value="{{$pesananproduk[0]['id_pesanan']}}"> @csrf </form>
			<div class="panel panel-headline">
				<div class="panel-heading">
					<a href="{{ route('order') }}" class="simple-button">Back</a>
					<a href="{{ route('order-ready') }}" class="simple-button" onclick="event.preventDefault(); document.querySelector('form#order-ready-form').submit();">Ubah Status: Pesanan Siap</a>
					<a href="{{ route('order-done') }}" class="simple-button" onclick="event.preventDefault(); document.querySelector('form#order-done-form').submit();">Ubah Status: Pesanan Selesai</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN CONTENT -->
@endsection