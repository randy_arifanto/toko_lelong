@extends('layouts/app')

@section('title', 'Kategori Management')

@section('content')
<!-- MAIN CONTENT -->
<div class="main">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Kategori Management') }}</h3>
					<p class="panel-subtitle"><a href="{{ route('kategori') }}">{{ __('Kategori List') }}</a> > {{ __('Add Kategori') }}</p>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				@foreach ($errors->all() as $error)
				<i class="fa fa-times-circle"></i> {{ $error }}<br>
				@endforeach
			</div>
			@endif
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Add Kategori') }}</h3>
				</div>
				<div class="panel-body">
					<form id="add-kategori-form" method="POST" action="{{ route('add-kategori') }}">
            			@csrf
						<label>{{ __('Nama Kategori') }}</label>
						<input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}">
					</form>
				</div>
			</div>
			<div class="panel panel-headline">
				<div class="panel-heading">
					<a href="{{ route('kategori') }}" class="simple-button">Cancel</a>
					<a href="{{ route('add-kategori') }}" class="simple-button" onclick="event.preventDefault(); document.querySelector('form#add-kategori-form').submit();">Submit</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN CONTENT -->
@endsection