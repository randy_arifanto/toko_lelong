@extends('layouts/app')

@section('title', 'Laporan Transaksi')

@section('pagespecificstyle')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('custom/vendor/chartist/css/chartist-custom.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
	.simple-button {
		color: #676A6D;
		border: 1px solid #676A6D;
		font-size: 15px;
		padding: 5px 12px;
		font-weight: normal;
		margin-right: 12px;
		display: inline-block;
		text-decoration: none;
	}
	.simple-button:hover, .simple-button:active {
		color:#fff;
		background:#2B333E;
	}

	@media print {
		body * {
			visibility: hidden;
		}
		.printable-area, *.printable-area * {
			visibility: visible;
		}		
		#wrapper .main {
			padding-top: 0;
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
		}		
		.printable-title {
			margin-bottom: 0;
		}
	}
</style>
@endsection

@section('content')
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel">{{ __('Pilih Rentang Tanggal') }}</h5>
      </div>
	  <form method="POST" action="{{ route('laporan') }}">
		@csrf
		<div class="modal-body">
			<label>{{ __('Silahkan Pilih Rentang Tanggal') }}</label>
			<input type="text" name="daterange_filter" class="form-control" placeholder="Pilih rentang tanggal">
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
			<button type="submit" class="btn btn-primary">{{ __('Filter Data') }}</button>
		</div>
	  </form>
    </div>
  </div>
</div>
<!-- MAIN CONTENT -->
<div class="main">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-headline printable-area printable-title">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Laporan Transaksi') }}</h3>
					<p class="panel-subtitle">Rentang Laporan: ({{ $filter_applied }})</p>
				</div>
			</div>
			<div class="panel panel-headline printable-area">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Tabel Laporan Transaksi') }}</h3>
				</div>
				<div class="panel-body">
					<!-- <div class="table-responsive"> -->
						<table id="example" class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Status</th>
									<th>Jumlah</th>
									<th>Total Harga</th>
								</tr>
							</thead>
							<tbody>
					        <?php $i = 1; ?>
					        @foreach($pesanan as $value)
								<tr>
									<td>{{ $i }}</td>
									<td>{{ $value['status'] }}</td>
									<td>{{ $value['total'] }}</td>
									<td>{{ $value['total_harga'] }}</td>
								</tr>
					        <?php $i++; ?>
							@endforeach
							</tbody>
						</table>
					<!-- </div> -->
				</div>
			</div>
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Diagram Laporan Transaksi') }}</h3>
				</div>
				<div class="panel-body">
						<div id="headline-chart" class="ct-chart"></div>
				</div>
			</div>
			<div class="panel panel-headline">
				<div class="panel-body">						
					<button class="simple-button" data-toggle="modal" data-target="#filterModal">{{ __('Filter Laporan') }}</button>
					<button onclick="window.print();" class="simple-button">{{ __('Print Laporan') }}</button>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@endsection

@section('pagespecificscript')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('custom/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('custom/vendor/chartist/js/chartist.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable({
			// "scrollX": true,
			"initComplete": function (settings, json) {  
				$("#example").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");            
			},
		});
		$('input[name="daterange_filter"]').daterangepicker();
	});
</script>
<script type="text/javascript">
	$(function() {
		var app_data = {!! $pesanan_json !!};
		var data, options;
		var label_data = [];
		var series_data = [];
		for (var axis_data of app_data) 
		{
			label_data.push(axis_data.status);
			series_data.push(axis_data.total);
		}
		data = {
			labels: label_data,
			series: [
				series_data,
			]
		};
		options = {
			height: 300,
			axisX: {
				showGrid: false
			},
		};
		new Chartist.Bar('#headline-chart', data, options);
	});
</script>
@endsection
