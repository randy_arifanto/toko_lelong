<!doctype html>
<html lang="en">

<head>
    <title>@yield('pagetitle')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{ asset('custom/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/vendor/linearicons/style.css') }}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('custom/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/css/custom.css') }}">
    @yield('pagespecificstyle')
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('custom/img/main_logo.jpeg') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('custom/img/main_logo.jpeg') }}">
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        @include('components/navbar')
        <!-- END NAVBAR -->

        <!-- LEFT SIDEBAR -->
        @include('components/left_sidebar')
        <!-- END LEFT SIDEBAR -->

        <!-- MAIN -->
        @yield('content')
        <!-- END MAIN -->
        <div class="clearfix"></div>
        @include('components/footer')
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="{{ asset('custom/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('custom/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('custom/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('custom/scripts/klorofil-common.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
	<script>
		$(document).on("wheel", "input[type=number]", function (e) {
			$(this).blur();
		});
	</script>
    @yield('pagespecificscript')
</body>

</html>
