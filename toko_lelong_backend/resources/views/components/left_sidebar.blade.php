<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{ route('order') }}"><i class="lnr lnr-cart"></i> <span>Order</span></a></li>
                <li><a href="{{ route('produk') }}"><i class="lnr lnr-diamond"></i> <span>Produk</span></a></li>
                <li><a href="{{ route('kategori') }}"><i class="lnr lnr-book"></i> <span>Kategori</span></a></li>
                <li><a href="{{ route('laporan') }}"><i class="lnr lnr-file-empty"></i> <span>Laporan Transaksi</span></a></li>
            </ul>
        </nav>
    </div>
</div>