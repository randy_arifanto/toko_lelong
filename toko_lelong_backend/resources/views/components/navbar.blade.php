<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="{{route('home')}}"><img src="{{ asset('custom/img/main_logo.png') }}" height="80" width="100" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('custom/img/profile_admin.png') }}" class="img-circle" alt="Avatar"><span>{{{Auth::guard('web')->user()->name}}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        <form id='logout-form' method="POST" action="{{ route('logout') }}"> @csrf </form>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.querySelector('form#logout-form').submit();"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>