<footer>
	<div class="container-fluid">
	    <p class="copyright">&copy; 2021 <a href="{{ route('home') }}" target="_blank">Mitrgyna Speciosa</a>. All Rights Reserved.</p>
	</div>
</footer>