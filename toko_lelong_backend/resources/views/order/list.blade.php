@extends('layouts/app')

@section('title', 'Order Management')

@section('pagespecificstyle')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
<style type="text/css">
	.simple-button {
		color: #676A6D;
		border: 1px solid #676A6D;
		font-size: 15px;
		padding: 5px 12px;
		font-weight: normal;
		margin-right: 12px;
		display: inline-block;
		text-decoration: none;
	}
	.simple-button:hover, .simple-button:active {
		color:#fff;
		background:#2B333E;
	}
</style>
@endsection

@section('content')
<!-- MAIN CONTENT -->
<div class="main">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<!-- OVERVIEW -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Order Management') }}</h3>
					<p class="panel-subtitle">{{ __('Order List') }}</p>
				</div>
			</div>
			<!-- BASIC TABLE -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">{{ __('Order List') }}</h3>
				</div>
				<div class="panel-body">
					<!-- <div class="table-responsive"> -->
						<table id="example" class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Kode Order</th>
									<th>Status</th>
									<th>Lihat Detail</th>
								</tr>
							</thead>
							<tbody>
					        <?php $i = 1; ?>
					        @foreach($order as $value)
								<tr>
									<td>{{ $i }}</td>
									<td>{{ $value['kode_order'] }}</td>
									<td>{{ $value['status'] }}</td>
									<td><a href="{{ route('view-order', ['id' => $value['id']]) }}"><i class="lnr lnr-eye"></i></a></td>
								</tr>
					        <?php $i++; ?>
							@endforeach
							</tbody>
						</table>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@endsection

@section('pagespecificscript')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable({
			// "scrollX": true,
			"initComplete": function (settings, json) {  
				$("#example").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");            
			},
		});
	});
</script>
@endsection
