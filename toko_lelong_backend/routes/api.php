<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\KeranjangController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);    
    Route::get('/unauthorized', [AuthController::class, 'unauthorized'])->name('unauthorized');    
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'produk'
], function ($router) {
    Route::get('/get', [ProdukController::class, 'getProduk']);    
    Route::get('/get-detail', [ProdukController::class, 'getDetailProduk']);    
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'pesanan'
], function ($router) {
    Route::get('/get', [PesananController::class, 'getPesanan']);    
    Route::get('/get-detail', [PesananController::class, 'getDetailPesanan']);    
    Route::post('/checkout', [PesananController::class, 'addPesanan']);    
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'kategori'
], function ($router) {
    Route::get('/get', [KategoriController::class, 'getKategori']);    
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'keranjang'
], function ($router) {
    Route::get('/get', [KeranjangController::class, 'getKeranjang']);    
    Route::post('/add', [KeranjangController::class, 'addKeranjang']);    
});
