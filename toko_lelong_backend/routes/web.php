<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\KategoriBEController;
use App\Http\Controllers\ProdukBEController;
use App\Http\Controllers\LaporanBEController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['middleware' => 'auth:web'], function () {
    //Order
    Route::get('/', [OrderController::class, 'index'])->name('home');
    Route::get('/order', [OrderController::class, 'index'])->name('order');
    Route::get('/order/view/{id}', [OrderController::class, 'view'])->name('view-order');
    Route::post('/order/pesanansiap', [OrderController::class, 'order_status_ready'])->name('order-ready');
    Route::post('/order/pesananselesai', [OrderController::class, 'order_status_done'])->name('order-done');

    //Produk
    Route::get('/produk', [ProdukBEController::class, 'index'])->name('produk');
    Route::get('/produk/add', [ProdukBEController::class, 'add'])->name('add-produk');
    Route::post('/produk/add', [ProdukBEController::class, 'addact']);
    Route::get('/produk/edit/{id}', [ProdukBEController::class, 'edit'])->name('edit-produk');
    Route::post('/produk/edit', [ProdukBEController::class, 'editact'])->name('edit-produk-act');

    //Kategori
    Route::get('/kategori', [KategoriBEController::class, 'index'])->name('kategori');
    Route::get('/kategori/add', [KategoriBEController::class, 'add'])->name('add-kategori');
    Route::post('/kategori/add', [KategoriBEController::class, 'addact']);
    Route::get('/kategori/edit/{id}', [KategoriBEController::class, 'edit'])->name('edit-kategori');
    Route::post('/kategori/edit', [KategoriBEController::class, 'editact'])->name('edit-kategori-act');

    //Laporan
    Route::get('/laporan', [LaporanBEController::class, 'index'])->name('laporan');
    Route::post('/laporan', [LaporanBEController::class, 'filter']);
    
    Route::post('/logout',[AdminAuthController::class, 'logout'])->name('logout');
});
Route::get('/login', [AdminAuthController::class, 'loginForm'])->name('login');
Route::post('/login',[AdminAuthController::class, 'login']);
