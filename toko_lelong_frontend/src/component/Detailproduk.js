import React, { Component } from 'react';
import axios from 'axios';

class Detailproduk extends Component {

  constructor(props) {
    super(props);
    this.state = {
      produk: {
        'gambar':'default.jpg'
      }
    };
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/produk/get-detail?i='+this.props.id_produk)
      .then(result => {
        const produk = result.data.data;
        this.setState({
          produk:produk
        });
      })
  }

  render() {
    var produk = this.state.produk
    var addCart = this.props.addCart
    return (
      <div id="fh5co-product" className="fh5co-bg-section">
        <div className="container">
          <div className="row">
            <div className="col-md-10 col-md-offset-1 animate-box fadeInUp animated-fast">
              <div className="row animate-box fadeInUp animated-fast">
                <div className="col-md-8 col-md-offset-2 text-center">
                    <img className="img-responsive" src={"http://localhost:8000/images/"+produk.gambar} style={{border:'1px solid rgba(0, 0, 0, 0.05)', padding:'10px', marginBottom:'20px'}}  alt="user"></img>
                </div>
              </div>
              <div className="row animate-box fadeInUp animated-fast">
                <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
                  <h2>{produk.nama}</h2>
                  <p>
                    <a href="#" onClick = {() => addCart(produk.id)} className="btn btn-primary btn-outline btn-lg">Add to Cart</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <span className="price">Rp. {produk.harga}</span>
              <h2>{produk.nama}</h2>
              <p>{produk.deskripsi}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Detailproduk;