import React, { Component } from 'react';
import axios from 'axios';

class Produk extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    var detailProduk = this.props.detailProduk
    return (
        <div id="fh5co-product" className="fh5co-bg-section">
            <div className="container">
                <div className="row animate-box fadeInUp animated-fast">
                    <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <span>Lugusi Market</span>
                        <h2>Barang Bintang Lima, Harga Kaki Lima</h2>
                        {this.props.page_description}
                    </div>
                </div>
                <div className="row">
                    {this.props.produk.map(produk => 
                      <div key={produk.id} className="col-md-4 text-center animate-box fadeInUp animated-fast">
                          <div className="product">
                              <div className="product-grid" style={{backgroundImage:'url("http://localhost:8000/images/'+produk.gambar+'")'}}>
                                  <div className="inner">
                                      <p>
                                          <a href="#" onClick = {() => detailProduk(produk.id)} className="icon"><i className="icon-eye"></i></a>
                                      </p>
                                  </div>
                              </div>
                              <div className="desc">
                                  <h3><a href="#" onClick = {() => detailProduk(produk.id)}>{produk.nama}</a></h3>
                                  <span className="price">Rp. {produk.harga}</span>
                              </div>
                          </div>
                      </div>
                    )}
                </div>
            </div>
        </div> 
    );
  }
}

export default Produk;