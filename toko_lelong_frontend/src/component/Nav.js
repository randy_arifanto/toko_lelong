import React, { Component } from 'react';
import axios from 'axios';
import Cookie from "js-cookie";

class Nav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      kategori: [],
      keyword: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/kategori/get')
    .then(result => {
        const kategori_list = result.data.data;
        this.setState({kategori:kategori_list});
    })
  }

  handleChange({ target }) {
    this.setState({
      [target.name]: target.value
    });
  }

  logout(e) {
    e.preventDefault();
    let self = this;
    let payload={
      "key":"value"
    }
    let token =  Cookie.get("token") ? Cookie.get("token") : null;
    if (token === null) {
        alert('please login')
        self.props.changeHalaman('home')
    } else {
        axios.post("http://127.0.0.1:8000/api/auth/logout", payload, {
            headers: {
              'Authorization': 'Bearer '+token
            }
        })
        .then(function (response) {
            Cookie.remove("token")
            Cookie.remove("user")
            self.props.loggedOut()
            alert(response.data.message)
            self.props.changeHalaman('home')
        })
        .catch(function (error) {
          if (typeof error.response !== 'undefined') {
            if(error.response.status === 409){
              alert(error.response.data.message)
            }
            if(error.response.status === 500){
              alert(error.response.data.message)
            } else {
              alert(error.response.statusText)
            }
          } else {
            console.log(error);
          }
        });
    }
  }

  render() {
    var changeKategori = this.props.changeKategori
    var cariProduk = this.props.cariProduk
    var changeHalaman = this.props.changeHalaman
    return (
        <nav className="fh5co-nav" role="navigation">
            <div className="container">
                <div className="row">
                    <div className="col-md-3 col-xs-2">
                        <div id="fh5co-logo"><a href="index.html">Lugusi Market.</a></div>
                    </div>
                    <div className="col-md-6 col-xs-6 text-center menu-1">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li className="has-dropdown">
                                <a href="#" onClick = {() => changeHalaman('home')}>Kategori</a>
                                <ul className="dropdown">
                                    {this.state.kategori.map(kategori => 
                                    <li key={kategori.id} ><a href="#" onClick = {() => changeKategori(kategori.id, kategori.nama)}>{kategori.nama}</a></li>
                                    )}
                                </ul>
                            </li>
                            {this.props.logged_in ? (
                            <>
                                <li><a href="#" onClick = {() => changeHalaman('order')}>Pesanan Saya</a></li>
                                <li><a href="#" onClick = {this.logout} >Logout</a></li>
                            </>
                            ) : (
                            <>                                
                                <li><a href="#" onClick = {() => changeHalaman('login')} >Login</a></li>
                                <li><a href="#" onClick = {() => changeHalaman('register')} >Register</a></li>
                            </>
                            )}
                        </ul>
                    </div>
                    <div className="col-md-3 col-xs-4 text-right hidden-xs menu-2">
                        <ul>
                            <li className="search">
                                <div className="input-group">
                                  <input 
                                    name="keyword" 
                                    value={this.state.keyword} 
                                    onChange={this.handleChange} 
                                    type="text" 
                                    placeholder="Search..">                                        
                                  </input>
                                  <span className="input-group-btn">
                                    <button className="btn btn-primary" onClick = {() => cariProduk(this.state.keyword)} type="button"><i className="icon-search"></i></button>
                                  </span>
                                </div>
                            </li>

                            {this.props.logged_in ? (
                            <>
                                <li className="shopping-cart"><a href="#" onClick = {() => changeHalaman('keranjang')} className="cart"><span><i className="icon-shopping-cart"></i></span></a></li>
                            </>
                            ) : (
                            <></>
                            )}
                        </ul>
                    </div>
                </div>                
            </div>
        </nav>    
    );
  }
}

export default Nav;