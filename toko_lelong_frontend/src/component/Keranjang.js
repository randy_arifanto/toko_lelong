import React, { Component } from 'react';
import axios from 'axios';
import Cookie from "js-cookie";

class Keranjang extends Component {

  constructor(props) {
    super(props);
    this.state = {
      produk: []
    };
  }

  componentDidMount() {
    let token =  Cookie.get("token") ? Cookie.get("token") : null;
    let self = this;
    if (token === null){
      self.props.changeHalaman('login')
    } else {
      axios.get('http://127.0.0.1:8000/api/keranjang/get', {
        headers: {
          'Authorization': 'Bearer '+token
        }
      })
      .then(result => {
        const produk_list = result.data.data;
        this.setState({
          produk:produk_list
        });
      })
    }
  }

  render() {
    var checkout = this.props.checkout
    var changeHalaman = this.props.changeHalaman
    return (
        <div id="fh5co-product" className="fh5co-bg-section">
            <div className="container">
                <div className="row animate-box fadeInUp animated-fast">
                    <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <span>Lugusi Market</span>
                        <h2>Keranjang Belanja Anda</h2>
                    </div>
                </div>
                <div className="row">
                    {this.state.produk.map(produk => 
                      <div key="{produk.id}" className="col-md-4 text-center animate-box fadeInUp animated-fast">
                          <div className="product">
                              <div className="product-grid" style={{backgroundImage:'url("http://localhost:8000/images/'+produk.gambar+'")'}}>
                              </div>
                              <div className="desc">
                                  <h3><a href="single.html">{produk.nama}</a></h3>
                                  <span className="price">Rp. {produk.harga}</span>
                              </div>
                          </div>
                      </div>
                    )}
                </div>
                <div className="row">
                  <div className="col-md-8 col-md-offset-2 text-center">
                    <a href="#" onClick = {() => changeHalaman("home")} className="btn btn-primary btn-outline btn-lg">Lanjut Belanja</a>
                    <a href="#" onClick = {() => checkout()} className="btn btn-primary btn-outline btn-lg">Checkout</a>
                  </div>
                </div>
            </div>
        </div> 
    );
  }
}

export default Keranjang;