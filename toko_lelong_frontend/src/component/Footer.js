import React, { Component } from 'react';
import axios from 'axios';

class Footer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
        <footer id="fh5co-footer" role="contentinfo">
            <div className="container">
                <div className="row copyright">
                    <div className="col-md-12 text-center">
                        <p>
                            <small className="block">&copy; 2018 Free HTML5. All Rights Reserved.</small> 
                            <small className="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://blog.gessato.com/" target="_blank">Gessato</a> &amp; <a href="http://unsplash.co/" target="_blank">Unsplash</a></small>
                        </p>
                    </div>
                </div>

            </div>
        </footer>    
    );
  }
}

export default Footer;