import React, { Component } from 'react';
import axios from 'axios';
import Cookie from "js-cookie";
import ReactModal from "react-modal";

class Order extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: [],
      showModal: false,
      detailOrder: [],
      kodeOrder: '',
      statusOrder: '',
      statusOrder: 0
    };
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal (detail, kode, status, total) {
    this.setState({ 
      showModal: true,
      detailOrder: detail,
      kodeOrder: kode,
      statusOrder: status,
      totalHarga: total
    });
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
  }

  componentDidMount() {
    let token =  Cookie.get("token") ? Cookie.get("token") : null;
    let self = this;
    if (token === null){
      self.props.changeHalaman('login')
    } else {
      axios.get('http://127.0.0.1:8000/api/pesanan/get', {
        headers: {
          'Authorization': 'Bearer '+token
        }
      })
      .then(result => {
        const order_list = result.data.data;
        this.setState({
          order:order_list
        });
      })
    }
  }

  render() {
    return (
        <div id="fh5co-product" className="fh5co-bg-section">
            <div className="container">
              <div className="row animate-box fadeInUp animated-fast">
                <div className="col-md-8 col-md-offset-2 text-center">
                  <span>Lugusi Market</span>
                  <h2>Pesanan Saya</h2>
                </div>
              </div>
              <div className="row">
                <div className="col-md-8 col-md-offset-2 text-center">
                  <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col" className="text-left">Tanggal Pemesanan</th>
                        <th scope="col" className="text-left">Kode Pesanan</th>
                        <th scope="col" className="text-left">Status Pesanan</th>
                        <th scope="col" className="text-left">Detail Pesanan</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.order.map(order =>
                        <tr>
                          <td className="text-left">{order.created_at}</td>
                          <td className="text-left">{order.kode_order}</td>
                          <td className="text-left">{order.status}</td>
                          <td className="text-left"><a href="#" onClick = {() => this.handleOpenModal(order.detail, order.kode_order, order.status, order.total_harga)} className="icon"><i className="icon-eye"></i></a></td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
              <ReactModal 
                 isOpen={this.state.showModal}
                 contentLabel="Minimal Modal Example"
              >
                <button onClick={this.handleCloseModal}>X</button>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col" className="text-left">Kode: {this.state.kodeOrder}</th>
                      <th scope="col" className="text-left">Status: {this.state.statusOrder}</th>
                    </tr>
                  </thead>
                </table>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col" className="text-left">Nama Barang</th>
                      <th scope="col" className="text-left">Jumlah</th>
                      <th scope="col" className="text-left">Harga</th>
                    </tr>
                  </thead>
                    <tbody>
                      {this.state.detailOrder.map(order =>
                        <tr>
                          <td className="text-left">{order.nama}</td>
                          <td className="text-left">{order.jumlah}</td>
                          <td className="text-left">Rp. {order.harga}</td>
                        </tr>
                      )}
                        <tr>
                          <td className="text-right" colSpan="2">Total Harga</td>
                          <td className="text-left">Rp. {this.state.totalHarga}</td>
                        </tr>
                    </tbody>
                </table>
              </ReactModal>
            </div>
        </div> 
    );
  }
}

export default Order;