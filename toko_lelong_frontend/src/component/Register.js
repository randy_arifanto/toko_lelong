import React, { Component } from 'react';
import axios from 'axios';

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name:'',
      email:'',
      password:'',
      password_confirmation:''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }  

  handleChange({target}) {
    this.setState({
      [target.name]: target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let self = this;
    let payload={
      "name":this.state.name,
      "email":this.state.email,
      "password":this.state.password,
      "password_confirmation":this.state.password_confirmation
    }
    axios.post("http://127.0.0.1:8000/api/auth/register", payload)
    .then(function (response) {
      self.props.changeHalaman('login')
    })
    .catch(function (error) {
      if (typeof error.response !== 'undefined') {
        if(error.response.status === 409){
          alert(error.response.data.message)
        }
        if(error.response.status === 500){
          alert(error.response.data.message)
        } else {
          alert(error.response.statusText)
        }
      } else {
        console.log(error);
      }
    });
  }

  render() {
    return (
        <div id="fh5co-contact" className="fh5co-bg-section">
          <div className="container">
            <div className="row">
              <div className="col-md-12 animate-box fadeInUp animated-fast">
                <h3>Halaman Registrasi</h3>
                <form action="#">
                  <div className="row form-group">
                    <div className="col-md-6">
                      <label htmlFor="nama">Nama</label>
                      <input 
                        type="text" 
                        name="name" 
                        className="form-control" 
                        value={this.state.name} 
                        onChange={this.handleChange} 
                        placeholder="Nama Anda">
                      </input>
                    </div>
                    <div className="col-md-6">
                      <label htmlFor="email">Email</label>
                      <input 
                        type="email" 
                        name="email" 
                        className="form-control" 
                        value={this.state.email} 
                        onChange={this.handleChange} 
                        placeholder="Email">
                      </input>
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-md-12">
                      <label htmlFor="password">Password</label>
                      <input 
                        type="password" 
                        name="password" 
                        className="form-control" 
                        value={this.state.password} 
                        onChange={this.handleChange} 
                        placeholder="Password Anda">
                      </input>
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-md-12">
                      <label htmlFor="password_confirmation">Ulangi Password</label>
                      <input 
                        type="password" 
                        name="password_confirmation" 
                        className="form-control" 
                        value={this.state.password_confirmation} 
                        onChange={this.handleChange} 
                        placeholder="Ulangi Password Anda">
                      </input>
                    </div>
                  </div>
                  <div className="form-group">
                    <button
                      className="btn btn-primary"
                      onClick={this
                      .handleSubmit}>
                      Daftar Sekarang
                    </button>
                  </div>

                </form>   
              </div>
            </div>
            
          </div>
        </div>
    );
  }
}

export default Register;