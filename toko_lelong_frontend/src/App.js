import React, { Component } from 'react';
import axios from 'axios';
import Nav from './component/Nav';
import Footer from './component/Footer';
import Produk from './component/Produk';
import Register from './component/Register';
import Login from './component/Login';
import Detailproduk from './component/Detailproduk';
import Keranjang from './component/Keranjang';
import Order from './component/Order';
import Cookie from "js-cookie";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      produk: [],
      jenis_produk: 'all',
      nama_kategori: 0,
      keyword: '',
      halaman: 'home',
      logged_in: Cookie.get("token") ? true : false,
      id_produk: 0
    };
    this.changeKategori = this.changeKategori.bind(this)
    this.cariProduk = this.cariProduk.bind(this)
    this.changeHalaman = this.changeHalaman.bind(this)
    this.loggedIn = this.loggedIn.bind(this)
    this.loggedOut = this.loggedOut.bind(this)
    this.detailProduk = this.detailProduk.bind(this)
    this.addCart = this.addCart.bind(this)
    this.checkout = this.checkout.bind(this)
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/produk/get')
      .then(result => {
        const produk_list = result.data.data;
        this.setState({
          produk:produk_list
        });
      })
  }

  changeKategori(id_kategori, nama_kategori) {
    axios.get('http://127.0.0.1:8000/api/produk/get?k='+id_kategori)
      .then(result => {
        const produk_list = result.data.data;
        this.setState({
          produk:produk_list
        });
    })
    this.setState({
      kategori:id_kategori,
      nama_kategori:nama_kategori,
      jenis_produk:'k',
      halaman:'home'
    });
  }

  changeHalaman(nama_halaman) {
    this.setState({
      halaman:nama_halaman
    });
  }

  detailProduk(id_produk) {
    console.log("id_produk")
    this.setState({
      halaman:"detail_produk",
      id_produk:id_produk
    });
  }

  addCart(id_produk) {
    let token =  Cookie.get("token") ? Cookie.get("token") : null;
    if (token === null){
      this.changeHalaman('login')
    } else {
        let payload={
            "id_produk":id_produk,
            "jumlah":1,
        }
        axios.post('http://127.0.0.1:8000/api/keranjang/add', payload, {
            headers: {
                'Authorization': 'Bearer '+token
            }
        })
        .then(result => {
            this.changeHalaman('keranjang')
        })
        .catch(function (error) {
            if (typeof error.response !== 'undefined') {
                if(error.response.status === 409){
                    alert(error.response.data.message)
                }
                if(error.response.status === 500){
                    alert(error.response.data.message)
                } else {
                    alert(error.response.statusText)
                }
            } else {
                console.log(error);
            }
        });
    }
  }

  checkout() {
    let token =  Cookie.get("token") ? Cookie.get("token") : null;
    if (token === null){
      this.changeHalaman('login')
    } else {
        let payload = {
            "empty":"empty",
        }
        axios.post('http://127.0.0.1:8000/api/pesanan/checkout', payload, {
            headers: {
                'Authorization': 'Bearer '+token
            }
        })
        .then(result => {
            this.changeHalaman('order')
        })
        .catch(function (error) {
            if (typeof error.response !== 'undefined') {
                if(error.response.status === 409){
                    alert(error.response.data.message)
                }
                if(error.response.status === 500){
                    alert(error.response.data.message)
                } else {
                    alert(error.response.statusText)
                }
            } else {
                console.log(error);
            }
        });
    }
  }

  loggedIn() {
    this.setState({
      logged_in:true
    });
  }

  loggedOut() {
    this.setState({
      logged_in:false
    });
  }

  cariProduk(keyword) {
    axios.get('http://127.0.0.1:8000/api/produk/get?q='+keyword)
      .then(result => {
        const produk_list = result.data.data;
        this.setState({
          produk:produk_list
        });
    })
    this.setState({
      keyword:keyword,
      jenis_produk:'q',
      halaman:'home'
    });
  }

  renderElement(){
    if (this.state.halaman == 'home'){
      const jenis_produk = this.state.jenis_produk;
      var detailProduk = this.detailProduk;
      let page_description
      if(jenis_produk == 'k') {
        page_description = <p>Produk dengan kategori "{this.state.nama_kategori}"</p>
      } else if (jenis_produk == 'q') {
        page_description = <p>Hasil pencarian untuk "{this.state.keyword}"</p>
      } else {
        page_description = <p>Semua produk kami.</p>
      }
      
      return (
          <Produk
            page_description = {page_description}
            produk = {this.state.produk}
            detailProduk = {detailProduk.bind(this)}
          />
      );
    } else if (this.state.halaman == 'register'){      
      var changeHalaman = this.changeHalaman;
      return (
          <Register 
            changeHalaman = {changeHalaman.bind(this)}
          />
      );
    } else if (this.state.halaman == 'login'){      
      var changeHalaman = this.changeHalaman;
      var loggedIn = this.loggedIn;
      return (
          <Login
            changeHalaman = {changeHalaman.bind(this)}
            loggedIn = {loggedIn.bind(this)}
          />
      );
    } else if (this.state.halaman == 'detail_produk'){
      var addCart = this.addCart;
      return (
          <Detailproduk
            id_produk = {this.state.id_produk}
            addCart = {addCart.bind(this)}
          />
      );
    } else if (this.state.halaman == 'keranjang'){
      var changeHalaman = this.changeHalaman;
      var checkout = this.checkout;
      return (
          <Keranjang
            changeHalaman = {changeHalaman.bind(this)}
            checkout = {checkout.bind(this)}
          />
      );
    } else if (this.state.halaman == 'order'){
      return (
          <Order
          />
      );
    }
    return null;
  }

  render() {
    var changeKategori = this.changeKategori;
    var cariProduk = this.cariProduk;
    var changeHalaman = this.changeHalaman;
    var loggedOut = this.loggedOut;
    return (
      <div id="page">
        <Nav 
          changeKategori = {changeKategori.bind(this)}
          cariProduk = {cariProduk.bind(this)}
          changeHalaman = {changeHalaman.bind(this)}
          loggedOut = {loggedOut.bind(this)}
          logged_in = {this.state.logged_in}
        />
        {this.renderElement()}
        <Footer />   
      </div>      
    );
  }
}

export default App;