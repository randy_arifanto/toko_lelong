const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');
const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/html/index.html",
  filename: "./index.html"
});
module.exports = {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, './public'),
    filename: 'main_bundle.js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader"
      }
    },
    {
      test: /\.css$/,
      use: ["style-loader", "css-loader"]
    }
    ]},
    plugins: [htmlPlugin]
  };